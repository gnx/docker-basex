FROM ubuntu:latest

MAINTAINER Hristo Hristov hristo.dr.hristov@gmail.com

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y upgrade && apt-get install -y basex && apt-get clean
